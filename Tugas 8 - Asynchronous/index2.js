var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 
let i = 0
function panggil(waktu) {
    if (i < books.length) {
        readBooksPromise(waktu,books[i])
        .then(sisaWaktu => panggil(sisaWaktu))
        .catch(error => console.log(error))
    }
    i++;
}

panggil(9000);