// di index.js
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Tulis code untuk memanggil function readBooks di sini
var i = 0;
function panggil(waktu){  
        if(i < books.length){
            readBooks(waktu,books[i], (waktuBaca) => panggil(waktuBaca))
        }
        i++;       
}
panggil(10000);