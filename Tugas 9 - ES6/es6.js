//Soal No.1

let golden = goldenFunction = () => console.log("This is golden!!")

golden;


console.log("");
console.log("");

//Soal No.2
const newFunction = (firstName, lastName) => {
    return {
        firstName,
        lastName,
        fullName () {
            return console.log(`${firstName} ${lastName}`)
                },
    }
}
newFunction("Robi Aurio", "Ureka").fullName()

console.log("");
console.log("");

//Soal No.3
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  };

const {firstName, lastName, destination, occupation, spell} = newObject;
console.log(firstName, lastName, destination, occupation, spell)

console.log("");
console.log("");

//Soal No.4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]
//Driver Code
console.log(combined)

console.log("");
console.log("");

//Soal No.5
const planet = "earth"
const view = "glass"
let before = `
    Lorem  ${view}  dolor sit amet, consectetur adipiscing elit,  ${planet}  do eiusmod tempor  
    incididunt ut labore et dolore magna aliqua. Ut enim 
    ad minim veniam
    `
 
// Driver Code
console.log(before)