//Soal No.1
function arrayToObject(arr) {
    var now = new Date()
    var thisYear = now.getFullYear() // 2020 (tahun sekarang) 
    if (arr[0] == undefined || arr[0] == null) {
        console.log("Tidak Ada")
   }
    for (let i = 0; i < arr.length; i++) {
        var arrObj = {
            firstName : arr[i][0],
            lastName : arr[i][1],
            gender : arr[i][2],
            age : thisYear-arr[i][3],
        }
        
         if(isNaN(arrObj.age) || arrObj.age <= 0){
            arrObj.age = "Invalid Birth Year"
            return console.log((i+1)+".",arrObj.firstName,' ',arrObj.lastName,' : ', arrObj)
        }
        else{
            console.log((i+1)+".",arrObj.firstName,' ',arrObj.lastName,' : ', arrObj)
        }
    }
}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
var people3= []
arrayToObject(people3) // ""
console.log("")
console.log("")
console.log("")
//Soal No.2
function shoppingTime(memberId, money) {
    // you can only write your code here! 
    var listBarang = [],sisa = money,tagihan=0;
    if (!memberId) {
        return "Mohon maaf, toko X hanya berlaku untuk member saja" 
    }
    else if (money < 50000){
        return "Mohon maaf, uang anda tidak cukup"
    }
    
        if (sisa >= 1500000) {
            tagihan += 1500000
            listBarang.push("Sepatu Stacattu")
        }
        if (sisa >= 500000){
            tagihan += 500000
            listBarang.push("Baju Zoro")
        }
        if (sisa >= 250000) {
            tagihan += 250000
            listBarang.push("Baju H&N")
        }
        if (sisa >= 175000){
            tagihan += 175000
            listBarang.push("Sweater Uniklooh")
        }
        if (sisa >= 50000){
            tagihan += 50000
            listBarang.push("Casing Handphone")
        }
        sisa -= tagihan;
    var pelanggan = {
        IdMember        : memberId,
        uang            : money,
        listPurchased   : listBarang,
        changeMoney     : sisa,
    };    
    return pelanggan;
}
   
  // TEST CASES
  console.log(shoppingTime('1820RzKrnWn08', 2475000));
    //{ memberId: '1820RzKrnWn08',
    // money: 2475000,
    // listPurchased:
    //  [ 'Sepatu Stacattu',
    //    'Baju Zoro',
    //    'Baju H&N',
    //    'Sweater Uniklooh',
    //    'Casing Handphone' ],
    // changeMoney: 0 }
  console.log(shoppingTime('82Ku8Ma742', 170000));
  //{ memberId: '82Ku8Ma742',
  // money: 170000,
  // listPurchased:
  //  [ 'Casing Handphone' ],
  // changeMoney: 120000 }
  console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
  console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
  console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja
  console.log("");
  console.log("");
  console.log("");


//Soal No.3
  function naikAngkot(arrPenumpang) {
    var arrDetilPenumpang = [];
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    if (arrPenumpang.length == 0) {
        return "[]"
    }    

    for (let i = 0; i < arrPenumpang.length; i++) {
        if ((arrPenumpang [i][1] == rute[0]&&arrPenumpang [i][2]== rute[5])) {
            tagihan = 10000;
        } 
        else if((arrPenumpang [i][1] == rute[0]&&arrPenumpang [i][2]== rute[4])
            ||  (arrPenumpang [i][1] == rute[1]&&arrPenumpang [i][2]== rute[5])){
            tagihan = 8000;
        }
        else if((arrPenumpang [i][1] == rute[0]&&arrPenumpang [i][2]== rute[3])
            ||  (arrPenumpang [i][1] == rute[1]&&arrPenumpang [i][2]== rute[4])
            ||  (arrPenumpang [i][1] == rute[2]&&arrPenumpang [i][2]== rute[5])){
            tagihan = 6000;
        }
        else if((arrPenumpang [i][1] == rute[0]&&arrPenumpang [i][2]== rute[2])
            ||  (arrPenumpang [i][1] == rute[1]&&arrPenumpang [i][2]== rute[3])
            ||  (arrPenumpang [i][1] == rute[2]&&arrPenumpang [i][2]== rute[4])
            ||  (arrPenumpang [i][1] == rute[3]&&arrPenumpang [i][2]== rute[5])){
            tagihan = 4000;
        }
        else if((arrPenumpang [i][1] == rute[0]&&arrPenumpang [i][2]== rute[1])
            ||  (arrPenumpang [i][1] == rute[1]&&arrPenumpang [i][2]== rute[2])
            ||  (arrPenumpang [i][1] == rute[2]&&arrPenumpang [i][2]== rute[3])
            ||  (arrPenumpang [i][1] == rute[3]&&arrPenumpang [i][2]== rute[4])
            ||  (arrPenumpang [i][1] == rute[4]&&arrPenumpang [i][2]== rute[5])){
            tagihan = 2000;
            
        }
        else{
            return "Anda Kurang Kerjaan"
            
        }
        var detilPelanggan = {
            Penumpang   :   arrPenumpang[i][0],
            NaikDari    :   arrPenumpang[i][1],
            Tujuan      :   arrPenumpang[i][2],
            bayar       :   tagihan,
             
        };
        arrDetilPenumpang.push(detilPelanggan);
    }
    return arrDetilPenumpang;
}
  //TEST CASE
  console.log(naikAngkot([['Dimitri', 'B', 'F'],['Icha', 'A', 'B']]));
  // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
   
  console.log(naikAngkot([])); //[]