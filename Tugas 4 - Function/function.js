//Soal No.1
function teriak() {
    return "Halo Sanbers!"
}
console.log(teriak());

//Soal No.2
console.log("");
var num1 = 12;
var num2 = 4;
function kalikan(angka1,angka2) {
    return angka1 * angka2;
} 
var hasilKali = kalikan(num1, num2);
console.log(hasilKali); // 48

//Soal No.3
console.log("");

var name = "Robi Aurio Ureka";
var age = 30;
var address = "Jln. Malioboro, Yogyakarta";
var hobby = "Gaming";
function introduce(nama,umur,alamat,hobi) {
    nama = name;
    umur = age;
    alamat = address;
    hobi = hobby;
    var kalimat = `Nama saya ${nama}, umur saya ${umur} tahun, alamat saya di ${alamat}, dan saya punya hobby yaitu ${hobi}!`;
    return kalimat;
}
var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan) // Menampilkan "Nama saya Agus, umur saya 30 tahun, alamat saya di Jln. Malioboro, Yogyakarta, dan saya punya hobby yaitu Gaming!" 
