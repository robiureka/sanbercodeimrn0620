//Soal Balik String

function balikString(kalimat) {
    var balikkalimat = '';
    for (let i = kalimat.length - 1; i >= 0; i--) {
        balikkalimat += kalimat[i];
    }
    return balikkalimat
}

//Soal Palindrome

function palindrome(kalimat) {
    var balikkalimat = '';
    for (let i = kalimat.length - 1; i >= 0; i--) {
        balikkalimat += kalimat[i];
    }
    if (kalimat == balikkalimat) {
        return true
    } else {
        return false
    }
}

//Soal Bandingkan Angka

function bandingkan(num1,num2) {
    if (num1 < 0 || num2 < 0 ) {
        return -1
    }
    else if (num1 == num2){
        return -1
    }
    else if (num2 == undefined){
        return num1
    }
    else if ( num1 > num2){
        return num1
    }
    else if (num1 < num2){
        return num2
    }
}

console.log(balikString("abcde")) // edcba
console.log(balikString("rusak")) // kasur
console.log(balikString("racecar")) // racecar
console.log(balikString("haji")) // ijah
console.log("")
console.log("")
// TEST CASES Palindrome
console.log(palindrome("kasur rusak")) // true
console.log(palindrome("haji ijah")) // true
console.log(palindrome("nabasan")) // false
console.log(palindrome("nababan")) // true
console.log(palindrome("jakarta")) // false
console.log("")
console.log("")
// TEST CASES Bandingkan Angka
console.log(bandingkan(10, 15)); // 15
console.log(bandingkan(12, 12)); // -1
console.log(bandingkan(-1, 10)); // -1 
console.log(bandingkan(112, 121));// 121
console.log(bandingkan(1)); // 1
console.log(bandingkan()); // -1
console.log(bandingkan("15", "18")) // 18