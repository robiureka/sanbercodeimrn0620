//Soal AscendingTen
function AscendingTen(num) {
    var numString ="";
    if (num != undefined) {
        for (let i = 0; i < 10; i++) {
            numString += `${num+i} `;
        }
        return numString;
    }
    else{
        return -1;
    }
}
//Soal DescendingTen
function DescendingTen(num) {
    var numString ="";
    if (num != undefined) {
        for (let i = 10; i > 0; i--) {
            numString += `${num-10+i} `;
        }
        return numString;
    }
    else{
        return -1;
    }
}

function ConditionalAscDesc(reference,check){
    if (check % 2 == 0) {
        var numString ="";
        if (reference != undefined) {
            for (let i = 10; i > 0; i--) {
                numString += `${reference-10+i} `;
            }
            return numString;
        }
        else{
            return -1;
        }
    }
    else if ( check % 2 == 1){
        var numString ="";
        if (reference != undefined) {
            for (let i = 0; i < 10; i++) {
                numString += `${reference+i} `;
            }
            return numString;
        }
        else{
            return -1;
        }
    }
    else{
        return -1
    }
}

//Soal Ular Tangga
function UlarTangga() {
    var ulartangga=100;
    var newulartangga = "";
    for (let i = 100; i > 0; i-= 10) 
    {
        newulartangga += `${i} ${i-1} ${i-2} ${i-3} ${i-4} ${i-5} ${i-6} ${i-7} ${i-8} ${i-9} \n`
    }
    return newulartangga
}
// TEST CASES Ascending Ten
console.log(AscendingTen(11)) // 11 12 13 14 15 16 17 18 19 20
console.log(AscendingTen(21)) // 21 22 23 24 25 26 27 28 29 30
console.log(AscendingTen()) // -1
console.log("")
console.log("")

console.log(DescendingTen(100)) // 100 99 98 97 96 95 94 93 92 91
console.log(DescendingTen(10)) // 10 9 8 7 6 5 4 3 2 1
console.log(DescendingTen()) // -1
console.log("")
console.log("")
// TEST CASES Conditional Ascending Descending
console.log(ConditionalAscDesc(20, 8)) // 20 19 18 17 16 15 14 13 12 11
console.log(ConditionalAscDesc(81, 1)) // 81 82 83 84 85 86 87 88 89 90
console.log(ConditionalAscDesc(31)) // -1
console.log(ConditionalAscDesc()) // -1
console.log("")
console.log("")


console.log(UlarTangga())