var nama;
var peran;

function soalIfElse (nama,peran){
    if (nama =='' && peran=='') {
        console.log("Nama harus diisi!")
    } 
    else if((nama == 'john' || nama == 'John' || nama == "JOHN") && peran == '') {
        console.log("Halo "+nama+", Pilih peranmu untuk memulai game!");    
    }
    else if((nama == 'jane' || nama == 'Jane' || nama == "JANE") && peran == 'Penyihir'){
        console.log("Selamat datang di Dunia Werewolf, "+nama)
        console.log("Halo Penyihir "+ nama +", kamu dapat melihat siapa yang menjadi werewolf!")
    }
    else if((nama == 'jenita' || nama == 'Jenita' || nama == "JENITA") && peran == 'Guard'){
        console.log("Selamat datang di Dunia Werewolf, "+nama)
        console.log("Halo Guard "+ nama +", kamu akan membantu melindungi temanmu dari serangan werewolf.")
    }
    else if((nama == 'junaedi' || nama == 'Junaedi' || nama == "JUNAEDI") && peran == 'Werewolf'){
        console.log("Selamat datang di Dunia Werewolf, "+nama)
        console.log("Halo Werewolf "+ nama +", Kamu akan memakan mangsa setiap malam!")
    }
}
soalIfElse('','');
console.log("");
soalIfElse('John','');
console.log("");
soalIfElse('Jane','Penyihir');
console.log("");
soalIfElse('Jenita','Guard');
console.log("");
soalIfElse('Junaedi','Werewolf');

console.log(" ");

var hari=1; 
var bulan=12; 
var tahun=2000;
var teksbulan;
switch (true) {
    case (hari < 1 || hari > 31):
        console.log("Inputan hari salah")
        break;
    case (tahun < 1900 || tahun > 2200):    
        console.log("Inputan tahun salah")
        break;
    case (bulan < 1 || bulan > 12):
        console.log("inputan bulan salah")
        break;
    default:
        switch (bulan) {
            case 1:
                teksbulan="Januari";
                console.log(hari+" "+teksbulan+" "+tahun);
                break;
            case 2:
                teksbulan="Februari";
                console.log(hari+" "+teksbulan+" "+tahun);
                break;
            case 3:
                teksbulan="Maret";
                console.log(hari+" "+teksbulan+" "+tahun);
                break;
            case 4:
                teksbulan="April";
                console.log(hari+" "+teksbulan+" "+tahun);
                break;
            case 5:
                teksbulan="Mei";
                console.log(hari+" "+teksbulan+" "+tahun);
                break;
            case 6:
                teksbulan="Juni";
                console.log(hari+" "+teksbulan+" "+tahun);
                break;
            case 7:
                teksbulan="Juli";
                console.log(hari+" "+teksbulan+" "+tahun);
                break;
            case 8:
                teksbulan="Agustus";
                console.log(hari+" "+teksbulan+" "+tahun);
                break;
            case 9:
                teksbulan="September";
                console.log(hari+" "+teksbulan+" "+tahun);
                break;
            case 10:
                teksbulan="Oktober";
                console.log(hari+" "+teksbulan+" "+tahun);
                break;
            case 11:
                teksbulan="November";
                console.log(hari+" "+teksbulan+" "+tahun);
                break;
            case 12:
                teksbulan="Desember";
                console.log(hari+" "+teksbulan+" "+tahun);
                break;        
            default:
                console.log("Tidak ada bulan seperti yang anda input")
                break;
        }
        break;
}


