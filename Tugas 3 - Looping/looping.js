//Soal No.1

var perulangan=2;
console.log("LOOPING PERTAMA");
while (perulangan <=20) {
    console.log(perulangan+" - I love coding");
    perulangan +=2;
}
console.log("");
console.log("LOOPING KEDUA");
perulangan=20;
while (perulangan >=2) {
    console.log(perulangan+" - I will become a mobile developer");
    perulangan-=2;
}

//Soal No.2
console.log("");
for (let perulangan2 = 1; perulangan2 <= 20; perulangan2++) {
    if (perulangan2%2 == 1 && perulangan2%3 ==0) {
        console.log(perulangan2+" - I love coding");
    }
    else if (perulangan2%2 == 0){
        console.log(perulangan2+" - Berkualitas");
    }
    else if (perulangan2%2 == 1){
        console.log(perulangan2+" - Santai");
    }
}
console.log("");
//Soal No.3
var horizontal="";
for (let perulangan3 = 0; perulangan3 < 4; perulangan3++) {
    var menyamping = 0
    while (menyamping <8) {
        horizontal += "#";
        menyamping++;
    }
    horizontal+='\n';
}
console.log(horizontal);

//Soal No.4
var menyamping2="";
console.log("");
for (let i = 1; i <= 7; i++) {
    for (let j = 0; j < i; j++) {
        menyamping2 += "#";
    }
    menyamping2 += '\n';
}
console.log(menyamping2);

//Soal No.5
console.log("");
var catur="";
for (let i = 0; i < 8; i++) {
    if (i%2==1) {
        console.log("# # # #");
    }
    else{
        console.log(" # # # #");
    }
}

//soal no.5 coba-coba
console.log("");
var catur="";
for (let i = 0; i < 8; i++) {
    for (let j = 0;j < 8;j++) {
        if (i%2==0) {
            if (j%2 == 0) {
                catur += " ";
            }
            else if (j%2 == 1){
                catur += "#";
            }
        }
        else if(i%2==1) {
            if (j%2 == 0) {
                catur += "#";
            }
            else if (j%2 == 1){
                catur += " ";
            }
        }
    }
    catur +='\n';
}
console.log(catur);