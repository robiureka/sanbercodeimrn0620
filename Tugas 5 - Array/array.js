//Soal No.1
function range(FirstNum,FinishNum) {
    var array= new Array();
    if (FirstNum > FinishNum) {
        for (let i = FirstNum; i >= FinishNum; i--) {
            array.push(i);
        }
        if (FirstNum == undefined || FinishNum == undefined) {
            return -1
        } else  {
            return array
        }    
    } else {
        for (let i = FirstNum; i <= FinishNum; i++) {
            array.push(i);
        }
        if (FirstNum == undefined || FinishNum == undefined) {
            return -1
        } else  {
            return array
        }    
    }
}
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 
console.log("");

//Soal No.2

function rangeWithStep(FirstNum,FinishNum,step) {
    var array= new Array();
    if (FirstNum > FinishNum) {
        for (let i = FirstNum; i >= FinishNum; i-=step) {
            array.push(i);
        }
        if (FirstNum == undefined || FinishNum == undefined) {
            return -1
        } else  {
            return array
        }    
    } else {
        for (let i = FirstNum; i <= FinishNum; i+=step) {
            array.push(i);
        }
        if (FirstNum == undefined || FinishNum == undefined) {
            return -1
        } else  {
            return array
        }    
    }
}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 
console.log("");


//Soal No.3
function sum(FirstNum,FinishNum,step) {
    var jumlah= 0;
    if (FirstNum > FinishNum) {
        if(step != undefined){
            for (let i = FirstNum; i >= FinishNum; i-=step) {
                jumlah += i;
            }
            return jumlah;
        }
        else if (step == undefined){
            for (let i = FirstNum; i >= FinishNum; i--) {
                jumlah += i;
            }
            return jumlah;
        } 
    } 
    
    else if (FirstNum < FinishNum) {
        if(step != undefined){
            for (let i = FirstNum; i <= FinishNum; i+=step) {
                jumlah += i;
            }
            return jumlah;
        }
        else if (step == undefined) {
            for (let i = FirstNum; i <= FinishNum; i++) {
                jumlah += i;
            }
            return jumlah;
        } 
    }

    else if (FirstNum != undefined) {
        return FirstNum
    }

    else {
        return 0;
    }
}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0
console.log("");

//Soal No.4

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]
function DataHandling(masukan) {
    var hasil = "";
    for (let i = 0; i < masukan.length; i++) {
        hasil +=    `
        Nomor ID        : ${masukan[i][1]}
        Nama Lengkap    : ${masukan[i][2]}
        TTL             : ${masukan[i][3]}
        Hobi            : ${masukan[i][4]}\n`
    }
    return hasil;
}
console.log(DataHandling(input))

//Soal No.5

function balikKata(kalimat) {
    var balikkalimat = '';
    for (let i = kalimat.length - 1; i >= 0; i--) {
        balikkalimat += kalimat[i];
    }
    return balikkalimat
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 
console.log("");

//Soal No.6
var input1 = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
function DataHandling2(inputan){
    inputan.splice(1,2,"Roman Alamsyah Elsharawy","Provinsi Bandar Lampung")
    inputan.splice(4, 1, "Pria", "SMA Internasional Metro")
    console.log(inputan);
    var TTL = inputan[3]
    var TTLbaru = TTL.split("/");
    var namabulan ="";
    switch (TTLbaru[1]) {
        case "01":
            namabulan = "Januari"            
            break;
        case "02":
            namabulan = "Februari"
             break;
        case "03":
            namabulan = "Maret"
             break;
        case "04":
            namabulan = "April"
             break;
        case "05":
            namabulan = "Mei"
             break;
        case "06":
            namabulan = "Juni"
             break;
        case "07":
            namabulan = "Juli"
             break;
        case "08":
            namabulan = "Agustus"
             break;
        case "09":
            namabulan = "September"
             break;
        case "10":
            namabulan = "Oktober"
             break;
        case "11":
            namabulan = "November"
             break;
        case "12":
            namabulan = "Desember"
             break;            
        default:
             "Bulan Tidak Ada"            
             break;
    }
    console.log(namabulan);
    var gabung = TTLbaru.join("-")
    console.log(gabung);
    TTLbaru.sort(function(a,b) {return b - a})
    console.log(TTLbaru);
    var nama = inputan[1];
    
    var namabaru=nama.slice(0,15)
    console.log(namabaru)
}
DataHandling2(input1);