/*
Soal No.1
Release 0
* */
class Animal {
    // Code class di sini
    constructor(name){
        this._name = name;
        this._cold_blooded = false;
        this._legs = 4;
    }
}
 
var sheep = new Animal("shaun");
 
console.log(sheep._name) // "shaun"
console.log(sheep._legs) // 4
console.log(sheep._cold_blooded) // false
console.log("")
console.log("")
console.log("")
/*
Soal No.1
Release 1
* */

// Code class Ape dan class Frog di sini
class Ape extends Animal{
    constructor(name){
        super(name);
    }
    get legs() {
        return this._legs;
    }
    set legs(kaki) {
        this._legs = kaki;
    }
    yell(){
        return console.log("Auoooo");
    }
} 

class Frog extends Animal{
    constructor(name){
        super(name);
    }
    get legs() {
        return this._legs;
    }
    set legs(kaki) {
        this._legs = kaki;
    }
    jump(){
        return console.log("hop hop");
    }
}

var sungokong = new Ape("kera sakti")
sungokong.legs = 2
console.log("Nama Kera nya "+sungokong._name)
console.log("Kaki kera ada "+sungokong.legs)
sungokong.yell() // "Auooo"
 console.log("");
var kodok = new Frog("buduk")
kodok.legs = 4
console.log("Nama kodok nya "+kodok._name)
console.log("Kaki kodok tetap "+kodok.legs)
kodok.jump() // "hop hop" 

console.log("");
console.log("");

//Soal No.2
//tanpa menggunakan template
// class Clock {
//     constructor(){
//         this.timer
//     }
    
  
//      render(){
//       var date = new Date();
  
//       var hours = date.getHours();
//       if (hours < 10) hours = '0' + hours;
  
//       var mins = date.getMinutes();
//       if (mins < 10) mins = '0' + mins;
  
//       var secs = date.getSeconds();
//       if (secs < 10) secs = '0' + secs;
  
//       var output = hours.toString()+ ":" +mins.toString()+ ":" +secs.toString()
  
//       console.log(output);
//      }
  
//     stop = function() {
//       clearInterval(this.timer);
//     };
  
//     start(){
//       this.render();
//       this.timer = setInterval(this.render, 1000);
//     };
  
//   }
  
//   var clock = new Clock();
//   clock.start(); 
  
  //Soal No.2
  //Menggunakan template
  class Clock1 {
    constructor({ template }){
        this._template = template
        this._timer
    }
  
    render() {
      var date = new Date();
  
      var hours = date.getHours();
      if (hours < 10) hours = '0' + hours;
  
      var mins = date.getMinutes();
      if (mins < 10) mins = '0' + mins;
  
      var secs = date.getSeconds();
      if (secs < 10) secs = '0' + secs;
  
      var output = this._template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);
  
      console.log(output);
    }
  
    stop() {
      clearInterval(this._timer);
    };
  
    start() {
      this.render();
      this.timer = setInterval(this.render.bind(this), 1000);
    };
  
  }
  
  var clock = new Clock1({template: 'h:m:s'});
  clock.start();