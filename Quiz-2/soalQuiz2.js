//Soal No.1

class Score {
    constructor (username,point,email) {
        this._point     = point
        this._username  = username
        this._email     = email
        
    }
    average(_point){
        
        let _jumlah = 0
        for (let i = 0; i < _point.length; i++) {
            _jumlah += _point[i]
            this._hasil = _jumlah/_point.length 
        }
        return  this._hasil
    }
    data(username,email){
        let datadiri = {
            username,
            "rata-rata" : this._hasil,
            email,
        }
        return datadiri
    }
}

let myscore = new Score;
myscore.average([100,90,80,50,55])
console.log(myscore.data("robi","robi@gmail.com"))
console.log("")
console.log("")
//Soal No.2
const data = [
  ["email", "quiz - 1", "quiz - 2", "quiz - 3"],
  ["abduh@mail.com", 78, 89, 90],
  ["khairun@mail.com", 95, 85, 88],
  ["bondra@mail.com", 70, 75, 78],
  ["regi@mail.com", 91, 89, 93]
]

viewScores = (data, subject) => {
  // code kamu di sini
  let arrPeserta = []; 
  for (let i = 1; i < data.length; i++) {
    let peserta = {
          email     : data[i][0],
          subject   : subject,
          point () {
              let nilai = 0
                if (subject == 'quiz-1') {
                  nilai = data[i][1]
                }
                else if (subject == 'quiz-2') {
                nilai = data[i][2]
                }
                else if (subject == 'quiz-3') {
                nilai = data[i][3]
                }
            return nilai
          },
      }
      arrPeserta.push(peserta);
  }
  return console.log(arrPeserta);
}

// TEST CASE
viewScores(data, "quiz-1")
viewScores(data, "quiz-2")
viewScores(data, "quiz-3")

//Soal no.3
recapScores = (data) => {
let tampilkan = ''
for (let i = 1; i < data.length; i++) {
        let email       = data[i][0]
        let average     = ((data[i][1] + data[i][2] + data[i][3])/3).toFixed(1)
        let predicate = ''
        
         average > 90 ? predicate = "Honour" : average > 80 ? 
         predicate = "Graduate" : average > 70 ? 
         predicate = "Participant" : "Tidak Lulus";
        tampilkan +=`
        ${i}.   Email           : ${email}\n
                Rata-Rata       : ${average}\n
                Predikat        : ${predicate}\n
                `
    }
    return console.log(tampilkan)
}

recapScores(data);