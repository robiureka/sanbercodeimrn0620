import React, { Component, version } from 'react';
import { 
    StyleSheet, 
    Image,
    View, 
    TouchableOpacity,
    Text,
    FlatList,
    TextInput,
    } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

export default class AboutScreen extends Component{
    render(){
        return (
        <View style = {styles.container}> 
            <View style = {styles.userDetail}>
                <Image source = {require('./Images/LogoProfile.png')} style = {{width : 120, height : 120}} />
                <Text style = {styles.userUsername}>Robi Aurio Ureka</Text>
            </View>
            <View style = {styles.userCategories}>
                <View style = {styles.userCategoriesTitle}>
                    <Text>Social Media</Text>
                </View>
                <View style = {styles.userItemList}>
                    <View style = {styles.ListItem}>
                        <Image source = {require('./Images/instagram1.png')} style={{width : 30, height : 30}}/>
                        <Text style = {styles.ListItemText}>robyureka</Text>
                    </View>
                    <View style = {styles.ListItem}>
                        <Image source = {require('./Images/FacebookLogo.png')} style={{width : 30, height : 30}}/>
                        <Text style = {styles.ListItemText}>robyureka</Text>
                    </View>
                    <View style = {styles.ListItem}>
                        <Image source = {require('./Images/TwitterLogo.png')} style={{width : 30, height : 30}}/>
                        <Text style = {styles.ListItemText}>robyureka</Text>
                    </View>
                </View>
            </View>
            <View style = {styles.userCategories}>
                <View style = {styles.userCategoriesTitle}>
                    <Text>Portfolio</Text>
                </View>
                <View style = {styles.userPortfolioList}>
                    <View style = {styles.ListPortfolio}>
                        <Image source = {require('./Images/gitHub.png')} style={{width : 35, height : 35}} />
                        <Text style = {styles.ListPortfolioText}>robiureka</Text>
                    </View>
                    <View style = {styles.ListPortfolio}>
                        <Image source = {require('./Images/gitLab.png')} style={{width : 35, height : 35}} />
                        <Text style = {styles.ListPortfolioText}>robiureka</Text>
                    </View>
                </View>
            </View>
        </View>
        )
    }
}


const styles = StyleSheet.create({
    container : {
        flex : 1,
        justifyContent : "space-evenly",
        alignItems : "center",
    },
    userDetail : {
        alignItems : "center",
    },
    userUsername : {
        paddingTop : 10,
        fontSize : 25,

    },
    userCategories : {
        width : 300,
        height : 150,
        borderRadius : 25,
        backgroundColor : '#e5e5e5',
        
    },
    userCategoriesTitle : {
        width : 300,
        height : 25,
        borderTopRightRadius : 25,
        borderTopLeftRadius : 25,
        fontSize : 18,
        fontWeight : "300",
        paddingBottom : 10,
        borderBottomWidth : 0.5,
        backgroundColor : '#EBAF56',
        alignItems : "center",
    },
    userItemList : {
        flex : 1,
        justifyContent : "center",
        paddingLeft : 25, 
    },
    ListItem : {
        flexDirection : "row",
        marginTop : 7,
    },
    ListItemText : {
        marginLeft : 20,
    },
    userPortfolioList : {
        marginTop : 35,
        flexDirection : "row",
        justifyContent : "space-evenly",
        alignItems : "center"
    },
    ListPortfolio : {
        justifyContent : "center",
        alignItems : "center"
    },
    ListPortfolioText : {
        marginTop : 10
    }
});