import React, { Component } from 'react';
import { View, Text, FlatList, TextInput, TouchableOpacity, StyleSheet, Image,ScrollView } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Skill from './skillList'
import skillData from './skillData.json'

export default class SkillScreen extends Component {
    render(){
        return(
            <ScrollView style = {styles.container}>
                <View style = {styles.header}>
                    <View style = {styles.logo}>
                        <Image source = {require('./Images/LogoApp.png')} style = {{width : 60,height : 60,borderRadius : 50,}} ></Image>
                    </View>
                    <View style = {styles.userGreet}>
                        <Icon name = "account-circle" size = {35}/>
                        <View >
                            <Text>Hai,</Text>
                            <Text>Robi Aurio Ureka</Text>
                        </View>
                    </View>
                </View>
                <View style = {styles.body}>
                    <Text style = {styles.bodyTitle}>Skills</Text>
                    {/* <Skill skill = {skillData.items[0]}/> */}
                    <FlatList 
                    data = {skillData.items}
                    renderItem ={(skillData) =><Skill skill = {skillData.item} />}
                    keyExtractor ={(item) => item.id}
                    // ItemSeparatorComponent={()=><View style = {{height : 0.5, backgroundColor : "#eee"}}/>}
                    />
                </View>
                
            </ScrollView>
        )
    }
}



const styles = StyleSheet.create({
    container : {
        flex : 1,
    },
    logo : {
        
        alignItems : "center",
        marginBottom : 15
    },
    header : {
        paddingTop : 30,
        paddingHorizontal : 15
        // backgroundColor : "#ec7541"
    },
    userGreet : {
        flexDirection : "row",
        paddingBottom : 10
    },
    body : {
        padding : 20
    },
    bodyTitle : {
        fontSize : 35,
        color : "#ec7549",
        borderBottomWidth : 2, 
        borderColor : "#ec7541"
    }
})
