import 'react-native-gesture-handler';
import React, { Component } from 'react';
import { StyleSheet, Text, View } from "react-native";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator} from '@react-navigation/drawer';
import { createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import LoginScreen from './LoginScreen';
import AboutScreen from './AboutScreen';
import SkillScreen from './SkillScreen';
import ProjectScreen from './ProjectScreen';
import AddScreen from './AddScreen';


const RootStack = createStackNavigator();
const Drawer = createDrawerNavigator();
const Tabs = createBottomTabNavigator();


// const RootStackScreen = () => (
//     <RootStack.Navigator>
//         <RootStack.Screen name ="Login" component = {LoginScreen}/>
//     </RootStack.Navigator>
// )

const TabScreen = () => (
    <Tabs.Navigator>
        <Tabs.Screen name ="SkillScreen" component = {SkillScreen}/>
        <Tabs.Screen name ="ProjectScreen" component = {ProjectScreen}/>
        <Tabs.Screen name ="AddScreen" component = {AddScreen}/>
    </Tabs.Navigator>
)

const DrawerScreen = () => (
    <Drawer.Navigator>
        <Drawer.Screen name = "TabScreen" component={TabScreen}/>
        <Drawer.Screen name = "AboutScreen" component={AboutScreen}/>
    </Drawer.Navigator>
)

export default class Indexs extends Component {
    render(navigation){
        return(
            <NavigationContainer>
                <RootStack.Navigator>
                    <RootStack.Screen name ="Login" component = {LoginScreen}/>
                    <RootStack.Screen name ="AboutScreen" component = {DrawerScreen}/>
                </RootStack.Navigator>
                
            </NavigationContainer>
            
        )
    }
}

