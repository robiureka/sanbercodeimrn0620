import React, { Component } from 'react';
import { View, Text, FlatList, TextInput, TouchableOpacity, StyleSheet, Image } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export default class Skill extends Component {
    render(){
        let skill = this.props.skill;
        return(
            <View style = {styles.container}>
                <View style = {styles.skill}>
                    <Icon name = {skill.iconName} size={100} style = {{color : "lightblue"}}  />
                    <View style = {styles.skillDetails}>
                        <Text style = {styles.skillTitle}>{skill.skillName}</Text>
                        <Text style = {styles.skillCategory}>{skill.categoryName}</Text>
                        <Text style = {styles.skillPercentage}>{skill.percentageProgress}</Text>
                    </View>
                    <TouchableOpacity style = {styles.skillArrow}>
                    <Icon name="arrow-right-circle" size={40} />
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container : {
        flex : 1,
    },
    skill : {
        borderRadius : 20,
        flexDirection : "row",
        marginTop : 15,
        width : "auto",
        padding : 20,
        backgroundColor : "#ec6941",
    },
    skillDetails : {
        paddingLeft : 15,
    },
    skillTitle : {
        fontSize : 23,
        
    },
    skillCategory : {
        fontSize : 13,
    },
    skillPercentage : {
        paddingLeft : "auto",
        fontSize : 42,
    },
    skillArrow : {
        justifyContent : "center",
        alignItems : "center",
        paddingLeft : 10,
        marginLeft : "auto"
        
    },
})