import React, { Component } from 'react';
import { 
    StyleSheet, 
    Image,
    View, 
    TouchableOpacity,
    Text,
    FlatList,
    } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import VideoItem from './Components/VideoItem';
import data from './data.json';


export default class Apps extends Component{
    render(){
        return (
            <View style = {styles.container}>
                <View style = {styles.navBar}>
                    <Image source = {require("./Images/logo.png")} style = {{width:98, height: 22}}></Image>
                    <View style = {styles.rightNav}>
                        <TouchableOpacity style = {styles.rightNavItem}>
                            <Icon name = "search" size ={25}></Icon>
                        </TouchableOpacity>
                        <TouchableOpacity style = {styles.rightNavItem}>
                        <Icon name = "account-circle" size ={25}></Icon>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style = {styles.body}>
                    
                    <FlatList
                    data = {data.items}
                    renderItem ={(video) =><VideoItem video = {video.item} />}
                    keyExtractor ={(item) => item.id}
                    ItemSeparatorComponent={()=><View style = {{height : 0.5, backgroundColor : "#e5e5e5"}}/>}
                    />
                       
                    
                </View>
                <View style = {styles.tabBar}>
                    <TouchableOpacity style = {styles.tabItems}>
                        <Icon name = "home" size = {25}></Icon>
                        <Text style = {styles.tabTitle}>Home</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style = {styles.tabItems}>
                        <Icon name = "whatshot" size = {25}></Icon>
                        <Text style = {styles.tabTitle}>Trending</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style = {styles.tabItems}>
                        <Icon name = "subscriptions" size = {25}></Icon>
                        <Text style = {styles.tabTitle}>Subscribe</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style = {styles.tabItems}>
                        <Icon name = "folder" size = {25}></Icon>
                        <Text style = {styles.tabTitle}>Library</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container : {
        flex : 1,   
    },
    navBar : {
        height : 55,
        backgroundColor : 'white',
        elevation : 3,
        paddingTop : 18,
        paddingHorizontal : 20,
        flexDirection : "row",
        alignItems : "center",
        justifyContent : "space-between",
    },
    
    rightNav : {
        flexDirection : "row",
    },

    rightNavItem : {
        marginLeft : 25,
    },

    body : {
        flex : 1,
    },

    tabBar : {
        backgroundColor : "white",
        height : 60,
        alignItems : "center",
        justifyContent : "space-around",
        borderTopWidth : 0.6,
        borderColor : "#E5E5E5",
        flexDirection : "row",
        
    },

    tabTitle : {
        fontSize : 11,
        color : "#3c3c3c",
        paddingTop : 5,
    },

    tabItems : {
        alignItems : "center",
        justifyContent : "center",
    },
})