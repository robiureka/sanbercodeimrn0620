import React, { Component } from 'react';
import { 
    StyleSheet, 
    Image,
    View, 
    TouchableOpacity,
    Text,
    FlatList,
    TextInput,
    } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

export default class LoginScreen extends Component{
    render(){
        return (
            <View style = {styles.container}>
                <View style = {styles.AppHeader}>
                    <Image source = {require('./Images/LogoApp.png')} style = {{width : 100,height : 100,borderRadius : 50,}}></Image>
                    <Text style = {styles.AppTitle}>Calibur App</Text>
                </View>
                <View style = {styles.loginForm}>
                    <View style = {styles.formItem}>
                        <Icon name = "account-circle" size={25}></Icon>
                        <TouchableOpacity>
                            <TextInput style = {styles.formInput} placeholder = "Input Your Username"/>
                        </TouchableOpacity>
                    </View>
                    <View style = {styles.formItem}>
                        <Icon name = "lock" size={25}></Icon>
                        <TouchableOpacity>
                            <TextInput style = {styles.formInput} placeholder = "Input Your Password"/>
                        </TouchableOpacity>
                    </View>
                </View>
                <TouchableOpacity style = {styles.formLogin}>
                    <Text style = {{fontSize : 25}}>Login</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container : {
        flex : 1,
        justifyContent : "space-evenly",
        alignItems : "center",
    },

    AppHeader : {
        padding : 17,
        alignItems : "center",
    },
    AppTitle : {
        paddingTop : 5,
        fontSize : 28,
        
    },
    loginForm : {

    },
    formItem : {
        flexDirection : "row",
        marginTop : 20,
        borderBottomWidth : 0.8,
    },
    formInput : {
        paddingLeft : 5,
        alignItems : "center",
        justifyContent : "center",
        width : 200,
    },
    formLogin : {
        width : 180,
        height : 50,
        borderRadius : 50,
        backgroundColor : '#EBAF56',
        alignItems : "center",
        justifyContent : "center",
        
    },
});